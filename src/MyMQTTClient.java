import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;

public class MyMQTTClient {
    MqttClient mqttClient;

    String brokerURL;
    String clientID;
    String username;
    String password;

    public MyMQTTClient(String brokerURL, String clientID, String username, String password) {
        this.brokerURL = brokerURL;
        this.clientID = clientID;
        this.username = username;
        this.password = password;
        try {
            initMQTTClient();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void initMQTTClient() throws MqttException {
        mqttClient = new MqttClient(brokerURL, clientID, null);
    }

    public void connect() throws MqttException {
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
        connOpts.setUserName(username);
        connOpts.setPassword(password.toCharArray());
        mqttClient.connect(connOpts);
    }

    public void publish(String topic, String message) throws MqttException {
        MqttMessage mqttMessage = new MqttMessage(message.getBytes());
        mqttClient.publish(topic, mqttMessage);
    }

    public void disconnect() throws MqttException {
        mqttClient.disconnect();
    }

}
