import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.UnsupportedEncodingException;
import java.util.Random;

public class Main {

    public final static String IBM_ORGANIZATION_ID = "zt3tev";

    public final static String IBM_DEVICE_TYPE = "testtype";
    public final static String IBM_DEVICE_ID = "testdevice";
    public final static String IBM_DEVICE_TOKEN = "88888888";

    public final static int IBM_PORT = 443;
    public final static String IBM_USERNAME = "use-token-auth";

    public final static String IBM_TOPIC_SCHEMA = "iot-2/evt/%s/fmt/%s";
    public final static String IBM_TOPIC_EVENT_ID = "status";
    public final static String IBM_TOPIC_FORMAT_JSON = "json";

    public final static String IBM_MESSAGE_JSON_SCHEMA = "{\"d\":%s}";
    public final static String IBM_MESSAGE_JSON_CONTENT_EXAMPLE = "{\"value\":5}";
    public final static String IBM_MESSAGE_JSON_CONTENT_SCHEMA = "{\"value\":%s}";

    private final static Random RANDOM = new Random();

    public static void main(String[] args){

        String brokerURL = "ssl://" + IBM_ORGANIZATION_ID + ".messaging.internetofthings.ibmcloud.com:" + IBM_PORT;
        String clientId = "d:" + IBM_ORGANIZATION_ID + ":" + IBM_DEVICE_TYPE + ":" + IBM_DEVICE_ID;
        String username = IBM_USERNAME;
        String password = IBM_DEVICE_TOKEN;

        MyMQTTClient myMQTTClient = new MyMQTTClient(brokerURL, clientId, username, password);

        try {
            System.out.println("Connecting to " + brokerURL +
                    "\nclientID: " + clientId +
                    "\nusername: " + username + " password: " + password);
            myMQTTClient.connect();
            System.out.println("Connected!");

            String topic = String.format(IBM_TOPIC_SCHEMA, IBM_TOPIC_EVENT_ID, IBM_TOPIC_FORMAT_JSON);
            //String message = String.format(IBM_MESSAGE_JSON_SCHEMA, IBM_MESSAGE_JSON_CONTENT_EXAMPLE);
            //myMQTTClient.publish(topic, message);

            for(int i=0; i<10; i++){
                int randomValue = RANDOM.nextInt(100);
                String content = String.format(IBM_MESSAGE_JSON_CONTENT_SCHEMA, randomValue);
                String message = String.format(IBM_MESSAGE_JSON_SCHEMA, content);
                System.out.println("Publishing to topic: " + topic + "\nmessage: " + message);
                myMQTTClient.publish(topic, message);
                System.out.println("Published!");
                Thread.sleep(1000);
            }

            System.out.println("Disconnecting...");
            myMQTTClient.disconnect();
            System.out.println("Disconnected!");
            System.exit(0);
        } catch (MqttException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

}
